export interface IHeadlineRevision {
    sub_title: string,
    title: string
}