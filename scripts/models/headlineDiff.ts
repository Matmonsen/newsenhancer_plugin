export interface IHeadlineDiff {
    title: string,
    sub_title: string
}