import Background from "./background";
import {messageTypes} from "../config/messageTypes";
import Log from "../util/debug";
import Notification from "../models/notification";
import Wordcloud from "./wordcloud";
import {ISubmission} from "../models/submission";

let wordcloud = new Wordcloud();
let background = new Background(wordcloud);
background.init();




chrome.runtime.onMessage.addListener(
function (request, sender, sendResponse) {
    Log.info("Message to background", request.type, request.data);
    switch (request.type) {
        case messageTypes.TAB_LOADED_URL:
            background.siteIsSupported(request.data, sendResponse);
            break;
        case messageTypes.GENERATE_WORDCLOUD_FOR_SITE:
            wordcloud.showWordCloudForLink(request.data);
            break;
        case messageTypes.NOTIFY_USER:
            Notification.notifyUser(request.data);
            break;
        case messageTypes.IS_SITE_SUPPORTED:
            background.siteIsSupported(request.data, sendResponse);
            break;
        case messageTypes.TOGGLE_CONTEXT_MENU:
            background.contextMenu.contextMenuElementNewsEnhancerId = request.data;
            break;
        case messageTypes.SUBMIT:
            let submissionData: ISubmission = request.data;
            background.submit(submissionData);
            break;
        case messageTypes.FETCH_ARTICLE:
            background.fetchArticle(request.data, sendResponse);
            break;
        case messageTypes.WORDCLOUD_FOR_ARTICLE:
            wordcloud.showWordCloudForLink(Object.assign(request.data, {tab: sender.tab}));
            break;
        case messageTypes.FETCH_INFO_FOR_LINKS:
            background.fetchHeadlineDataForLinks(request.data, sendResponse);
            break;
    }
    return true;
});

